lastElement <- 0;
myArray <- [
	"[#bae3ff]Remember to report [#ff0000]bugs [#bae3ff]and [#ff0000]crashes [#bae3ff] on the VC:MP forum [#ff6c00]and not here.",
	"[#bae3ff]You can use hydraulics by pressing [#ff6c00]NUM4, NUM8, NUM6, and NUM2.",
	"[#bae3ff]Check [#12d449]/help [#bae3ff]for a full list of commands you can use.",
	"[#bae3ff]Go to any [#ff6c00]Pay 'n' Spray [#bae3ff]to fix or tune your car.",
	"[#bae3ff]Press [#ff6c00]F7 [#bae3ff]to spawn ramps for stunting!",
	"[#bae3ff]Remember to report [#ff0000]bugs [#bae3ff]and [#ff0000]crashes [#bae3ff] on the VC:MP forum [#ff6c00]and not here.",
	"[#bae3ff]Return to the spawn screen by pressing [#ff6c00]F9.",
	"[#bae3ff]Beta testers are almost always on hand to help with any issues you may encounter.",
	"[#bae3ff]You can switch between windowed and fullscreen mode by pressing [#ff6c00]Alt [#bae3ff]+ [#ff6c00]Enter.",
	"[#bae3ff]Boost your car by pressing [#ff6c00]F8!",
	"[#bae3ff]Join the [#ff6c00]Last Man Standing [#bae3ff]competition - $10,000 prize! [#ff6c00](/lms)"
];

function Messages()
{
    local currentString = myArray[lastElement];
    lastElement++;

    if( lastElement > ( myArray.len() - 1 ) ) // array.len() - 1 would be the last index
        lastElement = 0; // reset to 0
	
	Message( currentString );
}

NewTimer( "Messages", 35000, 0 );