players <- {};
class CPlayer
{
	spawnWeps = {};
	pInstance = null;

	kills  = -1;
	deaths = -1;
	cash   = -1;

	constructor( cpInstance )
	{
		if( cpInstance )
		{
			this.pInstance = cpInstance;
			players.rawset( this.pInstance.ID, this );

			this.kills  = 0;
			this.deaths = 0;
			this.cash   = 2000;
			this.SetCash( 2000 );

			::ClientMessage( "[#f1f1f1]Welcome to [#ff8e1c]VL8 Team Deathmatch.", this.pInstance, 0, 0, 0 );
			::ClientMessage( "[#f1f1f1]Remember that you are playing on a [#ff8e1c]beta [#f1f1f1]version. Bugs are to be expected.", this.pInstance, 0, 0, 0 );
		}
	}

	function OnDeath()
	{
		this.AddCash( -50 );
	}

	function OnKill()
	{
		this.AddCash( 125 );
	}

	function AddCash( cash )
	{
		this.cash += cash;
		this.SetCash( this.cash );
	}

	function SetCash( cash )
	{
		this.cash = cash;
		this.pInstance.Cash = cash;
	}

	function SetSpawnWeapon( wepID )
	{
		this.spawnWeps.rawset( wepID, 20000 );
	}

	function GetSpawnWeapons()
	{
		foreach( wepID, wepAmmo in this.spawnWeps )
			this.pInstance.SetWeapon( wepID, wepAmmo );
	}
};