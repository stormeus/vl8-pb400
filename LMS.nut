const LMS_STATE_STOPPED = 0;
const LMS_STATE_WAITING = 1;
const LMS_STATE_STARTED = 2;

lmsPlayers <- {};
lmsVectors <- {};
lmsTeamIDs <- {};
lmsState   <- LMS_STATE_STOPPED;
lmsCount   <- 5;
lmsWeapon  <- 0;

function JoinLMS( player )
{
	if( lmsState == LMS_STATE_STARTED )
		return false;
	else if( player && player.IsSpawned )
	{
		if( lmsState == LMS_STATE_STOPPED )
		{
			local wep = random( 0, 6 );
			switch( wep )
			{
				case 0:
					lmsWeapon = 19;
					break;

				case 1:
					lmsWeapon = 20;
					break;

				case 2:
					lmsWeapon = 21;
					break;

				case 3:
					lmsWeapon = 26;
					break;

				case 4:
					lmsWeapon = 30;
					break;

				case 5:
					lmsWeapon = 32;
					break;

				case 6:
					lmsWeapon = 33;
					break;
			}


			Message( "[#ffb273]The " + GetWeaponName( lmsWeapon ) + " Last Man Standing competition is starting in 15 seconds!" );
			lmsState = LMS_STATE_WAITING;
			lmsCount = 5;

			NewTimer( "StartLMS", 15000, 1 );
		}
		
		Message( "[#ff8700]" + player.Name + " [#ffb273]has joined the Last Man Standing competition. [#ff8700](/lms)" );
		lmsPlayers.rawset( player.ID, player );
		lmsVectors.rawset( player.ID, player.Pos );
		lmsTeamIDs.rawset( player.ID, player.Team );

		Announce( player, "Last Man Standing", 4 );
		player.CanAttack = false;
		player.Pos       = Vector( -1502.054932, 1488.667725, 298.2204895 );
		
		return true;
	}
	else
		return null;
}

function StartLMS()
{
	if( lmsPlayers.len() < 3 )
	{
		foreach( plr in lmsPlayers )
		{
			local v = lmsVectors.rawget( plr.ID );

			plr.CanAttack = true;
			plr.Pos       = v;
		}

		lmsState = LMS_STATE_STOPPED;
		lmsPlayers.clear();
		lmsVectors.clear();
		Message( "[#ff4040]The LMS competition has been cancelled because less than three players joined." );
	}
	else
	{
		Message( "[#ff4040]LMS is starting. No more players may join.")
		lmsState = LMS_STATE_STARTED;
		foreach( plr in lmsPlayers )
		{
			plr.Disarm();
			plr.Team = 100 + plr.ID;
		}

		NewTimer( "LMSCountdown", 1000, 6 );
	}
}

function LMSCountdown()
{
	if( lmsState == LMS_STATE_STARTED )
	{
		local annMessage = lmsCount.tostring();
		if( lmsCount == 0 )
			annMessage = "-- GO! --";

		foreach( plr in lmsPlayers )
		{
			if( lmsCount >= 0 )
			{
				Announce( plr, "~b~--" + annMessage + "--", 6 );
				if( lmsCount == 0 )
				{
					plr.SetWeapon( lmsWeapon, 20000 );
					plr.CanAttack = true;
				}
			}
		}

		lmsCount--;
	}
}

function RemoveLMS( player )
{
	if( lmsPlayers.rawin( player.ID ) )
	{
		if( player.IsSpawned )
		{
			local pVec = lmsVectors.rawget( player.ID );
			player.Pos = pVec;
			if( lmsTeamIDs.rawin( player.ID ) )
				player.Team = lmsTeamIDs.rawget( player.ID );
		}

		player.CanAttack = true;
		lmsPlayers.rawdelete( player.ID );
		lmsVectors.rawdelete( player.ID );
		lmsTeamIDs.rawdelete( player.ID );
	}

	if( lmsPlayers.len() < 2 && lmsState == LMS_STATE_STARTED )
	{
		foreach( plr in lmsPlayers )
		{
			Message( "[#ff8700]" + plr.Name + " [#ffb273]won the LMS and the $10,000 prize!" );
			if( players.rawin( plr.ID ) )
			{
				local pPlr = players.rawget( plr.ID );
				pPlr.AddCash( 10000 );

				local pPos = lmsVectors.rawget( plr.ID );
				plr.Pos = pPos;

				if( lmsTeamIDs.rawin( plr.ID ) )
					plr.Team = lmsTeamIDs.rawget( player.ID );
			}
		}

		lmsState = LMS_STATE_STOPPED;
		lmsPlayers.clear();
		lmsVectors.clear();
		lmsTeamIDs.clear();
	}
}
