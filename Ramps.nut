function CreateObjectEx( model, world, pos, angle )
{
    local radians = angle * PI / 180;
	CreateObject( model, world, pos, 190 ).RotateToEuler( Vector( 0, 0, radians ), 0 );
}

function CreateStuntEnvironment()
{
	CreateObjectEx(560, 0, Vector( -1347.0647, -595.2531, 14.7221 ), 0.0525 );
	CreateObjectEx(560, 0, Vector( -1433.8448, -664.3201, 14.7224 ), 127.3830 );
	CreateObjectEx(560, 0, Vector( -1748.5547, -682.4487, 14.5524 ), 179.6786 );
	CreateObjectEx(560, 0, Vector( -1603.5370, -760.2050, 14.7216 ), 179.1564 );
	CreateObjectEx(560, 0, Vector( -1304.2778, -947.6393, 14.7223 ), 184.2241 );
	CreateObjectEx(560, 0, Vector( -1592.5592, -1292.0242, 14.7223 ), 77.7001 );
	CreateObjectEx(560, 0, Vector( -1748.9900, -724.6376, 14.7224 ), 0.5200 );
	CreateObjectEx(560, 0, Vector( -1610.6079, -991.5276, 14.7227 ), 170.0344 );
	CreateObjectEx(560, 0, Vector( -1738.2693, -1332.2828, 14.7224 ), 181.0809 );
	CreateObjectEx(560, 0, Vector( -1143.8638, -1222.3093, 14.7819 ), 254.4756 );
	CreateObjectEx(560, 0, Vector( -883.8796, -1223.0068, 11.7129 ), 158.7359 );
	CreateObjectEx(560, 0, Vector( -707.6725, -1374.6368, 10.9618 ), 22.3502 );
	CreateObjectEx(560, 0, Vector( -752.9549, -1247.8738, 10.9430 ), 199.4533 );
	CreateObjectEx(560, 0, Vector( -784.8286, -990.2865, 10.9255 ), 304.8458 );
	CreateObjectEx(560, 0, Vector( -973.9243, -907.8641, 13.1942 ), 39.9543 );
	CreateObjectEx(560, 0, Vector( -929.6346, -743.9721, 11.7584 ), 350.5344 );
	CreateObjectEx(560, 0, Vector( -998.2667, -477.9019, 10.6299 ), 6.9107 );
	CreateObjectEx(560, 0, Vector( -1208.3048, -365.7339, 10.6295 ), 4.8127 );
	CreateObjectEx(560, 0, Vector( -1127.3827, -164.5033, 11.2840 ), 0.3455 );
	CreateObjectEx(560, 0, Vector( -1045.5325, -228.4973, 11.3005 ), 283.2197 );
	CreateObjectEx(560, 0, Vector( -981.4769, 20.7354, 10.3910 ), 358.6747 );
	CreateObjectEx(560, 0, Vector( -1005.6885, 299.9139, 11.0962 ), 347.0118 );
	CreateObjectEx(560, 0, Vector( -686.2571, 286.4994, 10.9269 ), 259.6262 );
	CreateObjectEx(560, 0, Vector( -753.2859, 70.8920, 10.9424 ), 163.7260 );
	CreateObjectEx(560, 0, Vector( -787.1631, -11.2993, 10.9409 ), 333.0910 );
	CreateObjectEx(560, 0, Vector( -667.8743, 623.6464, 10.9379 ), 316.8625 );
	CreateObjectEx(560, 0, Vector( -859.8999, 788.3422, 10.9383 ), 205.9564 );
	CreateObjectEx(560, 0, Vector( -856.2393, 1162.3781, 10.6566 ), 341.4321 );
	CreateObjectEx(560, 0, Vector( -673.0257, 839.7388, 11.1172 ), 180.7346 );
	CreateObjectEx(560, 0, Vector( -383.1624, 875.5276, 10.9285 ), 337.2893 );
	CreateObjectEx(560, 0, Vector( -330.9164, 1145.9346, 9.2047 ), 157.4389 );
	CreateObjectEx(560, 0, Vector( -336.8161, 1085.9795, 20.6367 ), 270.3132 );
	CreateObjectEx(560, 0, Vector( -36.6646, 1046.4496, 10.7065 ), 147.6291 );
	CreateObjectEx(560, 0, Vector( 411.9310, 669.8504, 11.0918 ), 177.7214 );
	CreateObjectEx(560, 0, Vector( 355.0280, 492.5728, 11.4990 ), 80.5171 );
	CreateObjectEx(560, 0, Vector( 269.6098, 326.6879, 11.5108 ), 95.9958 );
	CreateObjectEx(560, 0, Vector( 11.1595, 145.9216, 18.9949 ), 131.5727 );
	CreateObjectEx(560, 0, Vector( 496.8752, -8.0877, 10.7556 ), 172.7558 );
	CreateObjectEx(560, 0, Vector( 480.3638, -118.8655, 10.2531 ), 354.7593 );
	CreateObjectEx(560, 0, Vector( -87.0154, -1003.7452, 10.1676 ), 191.2159 );
	CreateObjectEx(560, 0, Vector( -48.0296, -1132.3508, 10.1676 ), 12.0817 );
	CreateObjectEx(560, 0, Vector( 61.8302, -1317.2311, 10.1676 ), 158.7217 );
	CreateObjectEx(560, 0, Vector( 159.2135, -1361.0416, 10.1363 ), 337.2786 );
	CreateObjectEx(560, 0, Vector( 206.1624, -1444.5801, 10.7558 ), 162.0956 );
	CreateObjectEx(560, 0, Vector( 280.6848, -1125.1960, 10.7558 ), 349.7359 );
	CreateObjectEx(560, 0, Vector( 379.6044, -777.9440, 10.7560 ), 156.2626 );
	CreateObjectEx(560, 0, Vector( 501.3036, -475.1053, 10.7549 ), 1.0193 );
	CreateObjectEx(560, 0, Vector( 809.5753, -313.3156, 11.9422 ), 302.7834 );
	CreateObjectEx(560, 0, Vector( 798.3956, -20.6564, 13.2128 ), 135.0493 );
	CreateObjectEx(560, 0, Vector( 707.3480, 40.8518, 10.9351 ), 3.0863 );
	CreateObjectEx(560, 0, Vector( 750.8860, -197.2536, 9.9425 ), 184.3575 );
	CreateObjectEx(560, 0, Vector( 798.8645, -373.5658, 11.9274 ), 185.9180 );
	CreateObjectEx(560, 0, Vector( 702.9043, -568.8072, 10.8777 ), 153.5656 );
}