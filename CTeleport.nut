teleportPickups <- {};
class CTeleport
{
	fromPickup = -1;
	fromVector = null;

	toPickup   = -1;
	toVector   = null;

	constructor( cpFromVec, cpToVec )
	{
		this.fromVector = cpFromVec;
		this.toVector   = cpToVec;

		this.fromPickup = ::CreatePickup( 382, 1, 0, this.fromVector, 255, 0 );
		this.toPickup   = ::CreatePickup( 382, 1, 0, this.toVector, 255, 0 );

		teleportPickups.rawset( this.fromPickup.ID, this );
		teleportPickups.rawset( this.toPickup.ID, this );
	}
};