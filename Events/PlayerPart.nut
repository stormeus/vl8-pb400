function onPlayerPart( player, reason )
{
	if( autoRespawn.rawin( player.ID ) )
		autoRespawn.rawdelete( player.ID );

	if( justTeleported.rawin( player.ID ) )
		justTeleported.rawin( player.ID );

	if( lmsPlayers.rawin( player.ID ) )
		RemoveLMS( player );

	if( garageUsers.rawin( player.ID ) )
	{
		local pGarage = garageUsers.rawget( player.ID );
		pGarage.DepartUser();

		garageUsers.rawdelete( player.ID );
	}

	if( garageEntries.rawin( player.ID ) )
		garageEntries.rawdelete( player.ID );

	if( playerVehicles.rawin( player.ID ) )
	{
		local veh = playerVehicles.rawget( player.ID );
		veh.Delete();

		playerVehicles.rawdelete( player.ID );
	}

	if( players.rawin( player.ID ) )
		players.rawdelete( player.ID );

	ProcessSpecRemoval(player);
}