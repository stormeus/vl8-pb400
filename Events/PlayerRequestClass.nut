autoRespawn <- {};
function onPlayerRequestClass( player, classOffset )
{
	if(player.SpectateTarget == null)
		SPRITE_SPECHINT.ShowForPlayer(player);
	
	if( autoRespawn.rawin( player.ID ) )
		player.Spawn();
	else if(player.SpectateTarget == null)
	{
		switch( classOffset )
		{
			case 0:
				Announce( player, "Free-for-All", 1 );
				break;

			case 1:
				Announce( player, "~t~Strippers", 1 );
				break;

			case 2:
				Announce( player, "~p~Street Thugs", 1 );
				break;

			case 3:
				Announce( player, "~r~Rich Girls", 1 );
				break;

			case 4:
				Announce( player, "~g~Beach Girls", 1 );
				break;

			case 5:
				Announce( player, "~b~Police", 1 );
				break;

			case 6:
				Announce( player, "~y~Cabbies", 1 );
				break;

			case 7:
				Announce( player, "~w~Tourists", 1 );
				break;

			case 8:
				Announce( player, "~p~Rednecks", 1 );
				break;

			case 9:
				Announce( player, "~x~Undercover Police", 1 );
				break;

			case 10:
				Announce( player, "~b~Bikers", 1 );
				break;

			case 11:
				Announce( player, "~y~Construction Workers", 1 );
				break;

			default:
				Announce( player, "", 1 );
				break;
		}
	}
}