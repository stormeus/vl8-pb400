function onPlayerDeath( player, reason )
{
	if( players.rawin( player.ID ) )
	{
		local pPlr = players.rawget( player.ID );
		pPlr.OnDeath();
	}

	if( lmsPlayers.rawin( player.ID ) )
		RemoveLMS( player );

	if( garageUsers.rawin( player.ID ) )
	{
		local pGarage = garageUsers.rawget( player.ID );
		pGarage.DepartPlayer();

		garageUsers.rawdelete( player.ID );
	}

	player.Score = player.Score - 1;
	ProcessSpecRemoval(player);
}

function onPlayerKill( killer, player, reason, bodypart )
{
	if( players.rawin( player.ID ) )
	{
		local pPlr = players.rawget( player.ID );
		pPlr.OnDeath();
	}

	if( players.rawin( killer.ID ) )
	{
		local pKiller = players.rawget( killer.ID );
		pKiller.OnKill();
	}

	if( lmsPlayers.rawin( player.ID ) )
		RemoveLMS( player );

	if( reason == 39 ) // Vehicle
		PlaySound( killer.UniqueWorld, 4356, killer.Pos );

	if( garageUsers.rawin( player.ID ) )
	{
		local pGarage = garageUsers.rawget( player.ID );
		pGarage.DepartPlayer();

		garageUsers.rawdelete( player.ID );
	}

	player.Score = player.Score - 2;
	killer.Score = killer.Score + 2;
	ProcessSpecRemoval(player);
}

function onPlayerTeamKill( killer, player, reason, bodypart )
{
	if( killer.Team == 255 )
		onPlayerKill( killer, player, reason, bodypart );
	else
	{
		Message( "[#fa0909]Killing " + killer.Name + " for team-killing." );
		killer.Health = 0.0;

		if( lmsPlayers.rawin( player.ID ) )
			RemoveLMS( player );
	}

	if( garageUsers.rawin( player.ID ) )
	{
		local pGarage = garageUsers.rawget( player.ID );
		pGarage.DepartPlayer();

		garageUsers.rawdelete( player.ID );
	}

	killer.Score = killer.Score - 2;
	ProcessSpecRemoval(player);
}