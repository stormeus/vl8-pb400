function onPlayerChat( player, text )
{
	if( player.IsAdmin )
	{
		local color = player.Color.r;
		      color = (color << 8) + player.Color.g;
		      color = (color << 8) + player.Color.b;

		local tag = "Tester"
		switch( player.Name.tolower() )
		{
			case "maxorator":
			case "[ka]force":
			case "[ka]vrocker":
			case "[ka]adtec_224":
			case "[ka]juppi":
				tag = "Developer";
				break;

			default:
				tag = "Tester";
				break;
		}

		local msg = format(
			"[#ff7a00][%s] [#%x]%s[#ffffff]: %s",
			tag,
			color,
			player.Name,
			text
		);

		Message( msg );
		return 0;
	}

	return 1;
}