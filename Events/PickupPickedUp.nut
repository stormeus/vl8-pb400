justTeleported <- {};
function onPickupPickedUp( player, pickup )
{
	if( teleportPickups.rawin( pickup.ID ) )
	{
		local teleport = teleportPickups.rawget( pickup.ID );
		if( teleport.fromPickup.ID == pickup.ID )
		{
			justTeleported.rawset( player.ID, time() );
			player.Pos = teleport.toVector;
		}
		else if( teleport.toPickup.ID == pickup.ID )
		{
			justTeleported.rawset( player.ID, time() );
			player.Pos = teleport.fromVector;
		}
	}
	else if( garages.rawin( pickup.ID ) && player.Vehicle )
	{
		if( garageEntries.rawin( player.ID ) )
		{
			local lastEntry = garageEntries.rawget( player.ID );
			if( time() - lastEntry < 10 )
				return;
		}
		
		local pGarage = garages.rawget( pickup.ID );
		pGarage.EnterPlayer( player );
	}
}

function onPickupClaimPicked( player, pickup )
{
	if( justTeleported.rawin( player.ID ) && teleportPickups.rawin( pickup.ID ) )
	{
		local tpTime = justTeleported.rawget( player.ID );
		if( time() - tpTime >= 3 )
		{
			justTeleported.rawdelete( player.ID );
			return 0;
		}
	}

	return 1;
}