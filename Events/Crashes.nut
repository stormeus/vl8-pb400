function TXTAddLine(filename, text)
{
	local fhnd = file(filename, "a+");
	foreach(char in text)
	fhnd.writen(char, 'c');	
	fhnd.writen('\n', 'c');
	fhnd=null;
}

function onPlayerCrashDump(player, report)
{
	TXTAddLine("crashes/" + time().tostring() + "_" + player.Name + ".log", report);
}