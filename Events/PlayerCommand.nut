playerVehicles <- {};
playerBomb <- array(GetMaxPlayers(), 0);
staticPickup <- null;

function onPlayerCommand( player, command, arguments )
{
	if( arguments.len() < 1 )
		arguments = null;

	local pPlr;
	if( players.rawin( player.ID ) )
		pPlr = players.rawget( player.ID );
	else
		return 0;

	command = command.tolower();
	if( command == "help" || command == "commands" )
	{
		ClientMessage( "[#70d478]/spawnwep, wep, goto, stats, lms, leavelms, heal", player, 0, 0, 0 );
		ClientMessage( "[#70d478]/loc, hp, eject, ping, disarmme, skin, animhelp, veh", player, 0, 0, 0 );
		ClientMessage( "[#70d478]/dropveh, getveh, carcol, setcarcol, fps", player, 0, 0, 0 );
		ClientMessage( "[#70d478]NUM8, NUM4, NUM2, NUM6: [#e3d83d]faux hydraulics", player, 0, 0, 0 );
		ClientMessage( "[#70d478]F7: [#e3d83d]spawn ramp when in vehicle", player, 0, 0, 0 );
		ClientMessage( "[#70d478]F8: [#e3d83d]vehicle speed boost", player, 0, 0, 0 );
		ClientMessage( "[#70d478]F9: [#e3d83d]return to spawn screen", player, 0, 0, 0 );
	}
	/*else if( command == "buyc4" )
	{
		if( player.Cash < 3000 )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]Not enough money to buy a C4 bomb.", player, 0, 0, 0 );
		else {
			playerBomb[player.ID] = 1;
			ClientMessage( "[#70d478]F9: You have bought a C4 bomb. Plant it by pressing X. Detonate it by pressing Z.", player, 0, 0, 0 );
		}	
	}*/
	else if( command == "wep" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/wep weapon1 [#cfcfcf]OR [#e9e9e9]/wep weapon1,weapon2,weapon3,[...]", player, 0, 0, 0 );
		else
		{
			local weps = NumTok( arguments, "," );
			local i, wepID, w;
			for( i = 1; i <= weps; i++ )
			{
				w     = GetTok( arguments, ",", i );
				wepID = GetWeaponID( w );

				if( wepID == 255 )
				{
					try
					{
						wepID = w.tointeger();
					}
					catch( err ) { }
				}
				
				if( wepID )
				{
					if(( wepID >= 0 && wepID <= 33 ) || ( wepID >= 100 && wepID <= 119 ) || ( wepID >= 131 && wepID <= 132 ))
					{
						player.SetWeapon( wepID, 20000 );
						ClientMessage( "[#80eb7a]You have been given a " + GetWeaponName( wepID ) + ".", player, 0, 0, 0 );
					}
				}
			}
		}
	}
	else if( command == "disarmme" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.Disarm();
	}
	else if( command == "exec" )
	{
		if( player.IsAdmin )
		{
			if( arguments )
			{
				try {
					local script = compilestring( arguments );
					script();
				} catch(e) ClientMessage("[#ed4242]Error:[#FF0000] " + e,player,0,0,0 );
			} else ClientMessage("[#ff0000]No params !",player,0,0,0);
		} else ClientMessage("[#ff0000]You are not an admin !",player,0,0,0);
	}
	else if( command == "carcol" )
	{
		if( !player.Vehicle )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not in a vehicle.", player, 0, 0, 0 );
		else
			ClientMessage( "[#80eb7a]Colour 1 = " + player.Vehicle.Colour1 + ", Colour 2 = " + player.Vehicle.Colour2, player, 0, 0, 0 );
	}
	/*else if( command == "setalpha" )
	{
		if( !arguments || NumTok( arguments, " " ) < 2 )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]/setalpha [alpha] [fadetime]", player, 0, 0, 0 );
		else
		{
			local alpha = GetTok( arguments, " ", 1 );
			local fadetime = GetTok( arguments, " ", 2 );
			
			try
			{
				alpha = alpha.tointeger();
				fadetime = fadetime.tointeger();
				
				if( alpha < 0 )
					alpha = 0;
					
				if( fadetime < 1 )
					fadetime = 1;
					
				if( alpha > 255 )
					alpha = 255;
					
				player.SetAlpha( alpha, fadetime );
			}
			catch( e )
				ClientMessage("[#ed4242]Error: [#e9e9e9]Failure to set alpha.", player, 0, 0, 0 );
		}
	}*/
		else if( command == "setcarcol" )
	{
		if( !player.Vehicle )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not in a vehicle.", player, 0, 0, 0 );
		else if( !arguments || NumTok( arguments, " " ) < 2 )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/setcarcol [col1] [col2]", player, 0, 0, 0 );
		else
		{
			local col1 = GetTok( arguments, " ", 1 );
			local col2 = GetTok( arguments, " ", 2 );

			try
			{
				col1 = col1.tointeger();
				col2 = col2.tointeger();

				if( col1 < 0 )
					col1 = 0;

				if( col1 > 94 )
					col1 = 94;

				if( col2 < 0 )
					col2 = 0;

				if( col2 > 94 )
					col2 = 94;

				player.Vehicle.Colour1 = col1;
				player.Vehicle.Colour2 = col2;
				ClientMessage( "[#80eb7a]Done.", player, 0, 0, 0 );
			}
			catch( err )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]An invalid colour ID was specified.", player, 0, 0, 0 );
		}
	}
	else if( command == "goto" )
	{
		local plr;
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/goto [player name or ID]", player, 0, 0, 0 );
		else if( pPlr.cash < 500 )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You need at least $500 to teleport.", player, 0, 0, 0 );
		else if( plr = GetPlayer( arguments ) )
		{
			ClientMessage( "[#80eb7a]You will be teleported to " + plr.Name + " in five seconds.", player, 0, 0, 0 );
			local t = NewTimer( "Teleport", 5000, 1, player.ID, plr.Pos.x, plr.Pos.y, plr.Pos.z );

			pPlr.AddCash( -500 );
		}
		else
			ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.", player, 0, 0, 0 );
	}
	else if( command == "veh" )
	{
		if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/veh [car model ID]", player, 0, 0, 0 );
		else if( playerVehicles.rawin( player.ID ) )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You already have a vehicle. Use /dropveh.", player, 0, 0, 0 );
		else
		{
			try
			{
				local veh = CreateVehicle( arguments.tointeger(), 1, player.Pos, player.Angle, -1, -1 );
				if( veh )
				{
					playerVehicles.rawset( player.ID, veh );
					player.Vehicle = veh;
				}
				else
					ClientMessage( "[#ed4242]Error: [#e9e9e9]Could not create vehicle.", player, 0, 0, 0 );
			}
			catch( err )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]Invalid vehicle ID specified.", player, 0, 0, 0 );
		}
	}
	else if( command == "dropveh" )
	{
		if( !playerVehicles.rawin( player.ID ) )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You never spawned a vehicle with /veh", player, 0, 0, 0 );
		else
		{
			local veh = playerVehicles.rawget( player.ID );
			veh.Delete();

			playerVehicles.rawdelete( player.ID );
			ClientMessage( "[#5cc9ff]Done. You can spawn another vehicle with /veh", player, 0, 0, 0 );
		}
	}
	else if( command == "getveh" )
	{
		if( !playerVehicles.rawin( player.ID ) )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You never spawned a vehicle with /veh", player, 0, 0, 0 );
		else
		{
			local veh = playerVehicles.rawget( player.ID );
			if( veh )
			{
				veh.Pos        = player.Pos;
				player.Vehicle = veh;
			}
			
			ClientMessage( "[#5cc9ff]Done.", player, 0, 0, 0 );
		}
	}
	else if( command == "stats" )
	{
		local plr;
		if( !arguments )
		{
			local kills  = pPlr.kills;
			local deaths = pPlr.deaths;
			local ratio  = kills.tofloat() / deaths.tofloat();

			ClientMessage( "[#ff5ce7]" + player.Name + "'s Stats: [#5cc9ff]" + kills + " kills, " + deaths + " deaths, K/D ratio: " + ratio, player, 0, 0, 0 );
		}
		else if( !( plr = GetPlayer( arguments ) ) )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.", player, 0, 0, 0 );
		else
		{
			if( players.rawin( plr.ID ) )
			{
				local pRemotePlr = players.rawget( plr.ID );
				local kills  = pRemotePlr.kills;
				local deaths = pRemotePlr.deaths;
				local ratio  = kills.tofloat() / deaths.tofloat();

				ClientMessage( "[#ff5ce7]" + plr.Name + "'s Stats: [#5cc9ff]" + kills + " kills, " + deaths + " deaths, K/D ratio: " + ratio, player, 0, 0, 0 );
			}
		}
	}
	else if( command == "lms" )
	{
		local returnValue;
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
		{
			returnValue = JoinLMS( player );
			if( returnValue == false )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]LMS has already started. You cannot join mid-round.", player, 0, 0, 0 );
			else if( returnValue == null )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
			else
				ClientMessage( "[#80eb7a]You have joined the LMS competition! Use [#5cc9ff]/leavelms [#80eb7a]to leave.", player, 0, 0, 0 );
		}
	}
	else if( command == "leavelms" )
	{
		if( !lmsPlayers.rawin( player.ID ) )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not in the LMS round.", player, 0, 0, 0 );
		else
		{
			if( lmsState == LMS_STATE_WAITING )
				Message( "[#ff8700]" + player.Name + " [#ffb273]has [#ff8700]left [#ffb273]the LMS before it started." );
			else
				Message( "[#ff8700]" + player.Name + " [#ffb273]has [#ff8700]forfeited [#ffb273]the LMS match." );

			RemoveLMS( player );
		}
	}
	else if( command == "spawnwep" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/spawnwep weapon1 [#cfcfcf]OR [#e9e9e9]/spawnwep weapon1,weapon2,weapon3,[...]", player, 0, 0, 0 );
		else
		{
			local weps = NumTok( arguments, "," );
			local i, wepID, w;
			for( i = 1; i <= weps; i++ )
			{
				w     = GetTok( arguments, ",", i );
				wepID = GetWeaponID( w );

				if( wepID == 255 )
				{
					try
					{
						wepID = w.tointeger();
					}
					catch( err ) { }
				}
				
				if(( wepID >= 0 && wepID <= 33 ) || ( wepID >= 100 && wepID <= 119 ) || ( wepID >= 131 && wepID <= 132 ))
				{
					player.SetWeapon( wepID, 20000 );
					pPlr.SetSpawnWeapon( wepID );
					ClientMessage( "[#80eb7a]You will now spawn with a " + GetWeaponName( wepID ) + ".", player, 0, 0, 0 );
				}
			}
		}
	}
	else if( command == "heal" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( pPlr.cash < 250 )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You need at least $250 to heal.", player, 0, 0, 0 );
		else
		{
			ClientMessage( "[#80eb7a]You will be healed in three seconds.", player, 0, 0, 0 );
			NewTimer( "Heal", 3000, 1, player.ID );

			pPlr.AddCash( -250 );
		}
	}
	else if( command == "loc" )
	{
		local plr;
		if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/loc [player name or ID]", player, 0, 0, 0 );
		else if( plr = GetPlayer( arguments ) )
			ClientMessage( "[#80eb7a]" + plr.Name + " is in [#5cc9ff]" + GetDistrictName( plr.Pos.x, plr.Pos.y ) + ".", player, 0, 0, 0 );
		else
			ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.", player, 0, 0, 0 );
	}
	else if( command == "hp" || command == "health" )
	{
		local plr;
		if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/hp [player name or ID]", player, 0, 0, 0 );
		else if( plr = FindPlayer( arguments ) )
			ClientMessage( "[#80eb7a]" + plr.Name + " has [#5cc9ff]" + plr.Health + "% HP.", player, 0, 0, 0 );
		else
			ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.", player, 0, 0, 0 );
	}
	else if( command == "eject" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !player.Vehicle )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not in a vehicle.", player, 0, 0, 0 );
		else
		{
			player.Eject();
			ClientMessage( "[#80eb7a]You have been ejected.", player, 0, 0, 0 );
		}
	}
	else if( command == "ping" )
	{
		local plr;
		if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/ping [player name or ID]", player, 0, 0, 0 );
		else if( plr = GetPlayer( arguments ) )
			ClientMessage( "[#80eb7a]" + plr.Name + "'s Ping: [#5cc9ff]" + plr.Ping + "ms.", player, 0, 0, 0 );
		else
			ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.", player, 0, 0, 0 );
	}
	else if( command == "fps" )
	{
		local plr;
		if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/fps [player name or ID]", player, 0, 0, 0 );
		else if( plr = GetPlayer( arguments ) )
			ClientMessage( "[#80eb7a]" + plr.Name + "'s Framerate: [#5cc9ff]" + plr.FPS + " frames per second.", player, 0, 0, 0 );
		else
			ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.", player, 0, 0, 0 );
	}
	else if( command == "skin" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/skin [skin ID from 0 to 190]", player, 0, 0, 0 );
		else
		{
			try
			{
				local iSkin = arguments.tointeger();
				if( iSkin >= 0 && iSkin <= 190 )
				{
					switch( iSkin )
					{
						case 73:
						case 12:
						case 51:
						case 53:
						case 57:
						case 66:
						case 74:
						case 81:
						case 85:
						case 103:
						case 93:
						case 61:
							ClientMessage( "[#ed4242]Error: [#e9e9e9]You cannot use that skin as it is used by another class.", player, 0, 0, 0 );
							break;

						default:
							player.Skin = iSkin;
							ClientMessage( "[#80eb7a]Your skin has been set to #[#5cc9ff]" + iSkin, player, 0, 0, 0 );
							break;
					}
				}
				else
					ClientMessage( "[#ed4242]Usage: [#e9e9e9]/skin [skin ID from [#ed4242]0 to 190[#e9e9e9]]", player, 0, 0, 0 );
			}
			catch( err )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]Invalid skin ID entered.", player, 0, 0, 0 );
		}
	}
	else if( command == "animhelp" )
	{
		ClientMessage( "[#70d478]/detonate, /turn, /aim, /handsup, /flipoff, /phoneout, /phonein, /phone", player, 0, 0, 0 );
		ClientMessage( "[#70d478]/sell, /buy, /cpr, /sunbathe, /stretch, /watch, /dance, /riot", player, 0, 0, 0 );
	}
	else if( command == "detonate" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 62 );
	}
	else if( command == "turn" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 154 );
	}
	else if( command == "aim" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 155 );
	}
	else if( command == "handsup" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 161 );
	}
	else if( command == "flipoff" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 163 );
	}
	else if( command == "phoneout" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 165 );
	}
	else if( command == "phonein" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 164 );
	}
	else if( command == "phone" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 166 );
	}
	else if( command == "sell" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 11 );
	}
	else if( command == "buy" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 0, 171 );
	}
	else if( command == "cpr" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 24, 214 );
	}
	else if( command == "sunbathe" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 25, 210 );
	}
	else if( command == "watch" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else
			player.SetAnim( 26, 216 );
	}
	else if( command == "pxii" )
	{
		if( staticPickup == null )
			staticPickup = CreatePickup( 386, 1, 1, player.Pos, 255, false );
		else
			staticPickup.Pos = player.Pos;
	}
	else if( command == "stretch" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/stretch [1-3]", player, 0, 0, 0 );
		else
		{
			try
			{
				local id = arguments.tointeger();
				switch( id )
				{
					case 1:
						player.SetAnim( 26, 215 );
						break;

					case 2:
						player.SetAnim( 26, 217 );
						break;
						
					case 3:
						player.SetAnim( 26, 218 );
						break;
						
					default:
						ClientMessage( "[#ed4242]Usage: [#e9e9e9]/stretch [#ed4242][1-3]", player, 0, 0, 0 );
						break;
				}
			}
			catch( err )
			{
				ClientMessage( "[#ed4242]Error: [#e9e9e9]Invalid number entered.", player, 0, 0, 0 );
			}
		}
	}
	else if( command == "dance" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/dance [1-7]", player, 0, 0, 0 );
		else
		{
			try
			{
				local id = arguments.tointeger();
				switch( id )
				{
					case 1:
						player.SetAnim( 28, 226 );
						break;

					case 2:
						player.SetAnim( 28, 227 );
						break;
						
					case 3:
						player.SetAnim( 28, 228 );
						break;

					case 4:
						player.SetAnim( 28, 229 );
						break;

					case 5:
						player.SetAnim( 28, 230 );
						break;

					case 6:
						player.SetAnim( 28, 231 );
						break;

					case 7:
						player.SetAnim( 28, 232 );
						break;
						
					default:
						ClientMessage( "[#ed4242]Usage: [#e9e9e9]/dance [#ed4242][1-3]", player, 0, 0, 0 );
						break;
				}
			}
			catch( err )
			{
				ClientMessage( "[#ed4242]Error: [#e9e9e9]Invalid number entered.", player, 0, 0, 0 );
			}
		}
	}
	else if( command == "riot" )
	{
		if( !player.IsSpawned )
			ClientMessage( "[#ed4242]Error: [#e9e9e9]You are not spawned.", player, 0, 0, 0 );
		else if( !arguments )
			ClientMessage( "[#ed4242]Usage: [#e9e9e9]/riot [1-7]", player, 0, 0, 0 );
		else
		{
			try
			{
				local id = arguments.tointeger();
				switch( id )
				{
					case 1:
						player.SetAnim( 27, 219 );
						break;

					case 2:
						player.SetAnim( 27, 220 );
						break;
						
					case 3:
						player.SetAnim( 27, 221 );
						break;

					case 4:
						player.SetAnim( 27, 222 );
						break;

					case 5:
						player.SetAnim( 27, 223 );
						break;

					case 6:
						player.SetAnim( 27, 224 );
						break;

					case 7:
						player.SetAnim( 27, 225 );
						break;
						
					default:
						ClientMessage( "[#ed4242]Usage: [#e9e9e9]/riot [#ed4242][1-3]", player, 0, 0, 0 );
						break;
				}
			}
			catch( err )
			{
				ClientMessage( "[#ed4242]Error: [#e9e9e9]Invalid number entered.", player, 0, 0, 0 );
			}
		}
	}
	/*else if( command == "c4" )
	{
		print(player.World);
		print(player.Pos);
		local obj = CreateObject( 6054, player.World, player.Pos, 255 );
		print(obj);
		print(obj.ID);
		print(obj.Model);
	}*/
	else if( command == "c" )
		ClientMessage( "[#80eb7a]Do not use /c at the beginning of commands. Use /command instead.", player, 0, 0, 0 );
	else if( command == "auth" )
	{
		if( arguments == "adme|/|pl5!" )
		{
			player.IsAdmin = true;
			ClientMessage( "[#80eb7a]You have been authenticated as a beta tester.", player, 0, 0, 0 );
		}
	}
	else if( player.IsAdmin )
	{
		if( command == "ahelp" )
		{
			ClientMessage( "[#70d478]/tchat, /kick, /ban, /bring, /getip, /agoto, /v, /setadmin, /setweather", player, 0, 0, 0 );
			ClientMessage( "[#70d478]/setgravity, /setwater, /makecarimmune, /unmakecarimmune, /setcash", player, 0, 0, 0 );
		}
		else if( command == "unlock" )
			SetPassword("");
		else if( command == "tchat" )
		{
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/tchat [message]", player, 0, 0, 0 );
			else
			{
				foreach( plr in players )
				{
					if( plr.pInstance.IsAdmin )
						ClientMessage( "[#ff7a00][Tester Chat] [#e9e9e9]" + player.Name + ": " + arguments, plr.pInstance, 0, 0, 0 );
				}
			}
		}
		else if( command == "kick" )
		{
			local plr;
			if( !arguments || NumTok( arguments, " " ) < 2 )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/kick [player] [reason]", player, 0, 0, 0 );
			else if( !( plr = GetPlayer( GetTok( arguments, " ", 1 ) ) ) )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.")
			else
			{
				local reason = GetTok( arguments, " ", 2, NumTok( arguments, " " ) );
				Message( "[#fa0000]Admin " + player.Name + " kicked " + plr.Name + " for " + reason );

				KickPlayer( plr );
			}
		}
		else if( command == "ban" )
		{
			local plr;
			if( !arguments || NumTok( arguments, " " ) < 2 )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/ban [player] [reason]", player, 0, 0, 0 );
			else if( !( plr = GetPlayer( GetTok( arguments, " ", 1 ) ) ) )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.")
			else
			{
				local reason = GetTok( arguments, " ", 2, NumTok( arguments, " " ) );
				Message( "[#fa0000]Admin " + player.Name + " banned " + plr.Name + " for " + reason );

				BanPlayer( plr );
			}
		}
		else if( command == "bring" )
		{
			local plr;
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/bring [player]", player, 0, 0, 0 );
			else if( !( plr = GetPlayer( arguments ) ) )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.")
			else
			{
				Message( "[#fa0000]Admin " + player.Name + " teleported " + plr.Name + " to them." );
				plr.Pos = player.Pos;
			}
		}
		else if( command == "getip" )
		{
			local plr;
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/getip [player]", player, 0, 0, 0 );
			else if( !( plr = GetPlayer( arguments ) ) )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.")
			else
				ClientMessage( "[#80eb7a]" + plr.Name + "'s IP address: [#5cc9ff]" + plr.IP, player, 0, 0, 0 )
		}
		else if( command == "agoto" )
		{
			local plr;
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/agoto [player]", player, 0, 0, 0 );
			else if( !( plr = GetPlayer( arguments ) ) )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.")
			else
			{
				Message( "[#fa0000]Admin " + player.Name + " admin-teleported to " + plr.Name + "." );
				player.Pos = plr.Pos;
			}
		}
		else if( command == "setweather" )
		{
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/setweather [weather ID]", player, 0, 0, 0 );
			else
			{
				local weather = arguments.tointeger();
				SetWeather( weather );

				ClientMessage( "[#80eb7a]Set weather to ID #[#5cc9ff]" + weather + ".", player, 0, 0, 0 );
			}
		}
		else if( command == "setgravity" )
		{
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/setgravity [gravity]", player, 0, 0, 0 );
			else
			{
				local gravity = arguments.tofloat();
				SetGravity( gravity );

				ClientMessage( "[#80eb7a]Set gravity to #[#5cc9ff]" + gravity + ".", player, 0, 0, 0 );
			}
		}
		else if( command == "setwater" )
		{
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/setwater [water level]", player, 0, 0, 0 );
			else
			{
				local water = arguments.tofloat();
				SetWaterLevel( water );

				ClientMessage( "[#80eb7a]Set water level to #[#5cc9ff]" + water + ".", player, 0, 0, 0 );
			}
		}
		else if( command == "makecarimmune" )
		{
			local v;
			if( !player.Vehicle )
				ClientMessage( "[#ed4242]Error: You are not in a vehicle.", player, 0, 0, 0 );
			else
			{
				player.Vehicle.Immunity = 255;
				ClientMessage( "[#80eb7a]Your car is now completely immune to damage.", player, 0, 0, 0 );
			}
		}
		else if( command == "unmakecarimmune" )
		{
			local v;
			if( !player.Vehicle )
				ClientMessage( "[#ed4242]Error: You are not in a vehicle.", player, 0, 0, 0 );
			else
			{
				player.Vehicle.Immunity = 0;
				ClientMessage( "[#80eb7a]Your car is now completely vulnerable to damage.", player, 0, 0, 0 );
			}
		}
		else if( command == "v" )
		{
			if( !arguments )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/v [model ID]", player, 0, 0, 0 );
			else
			{
				try
				{
					player.Vehicle = CreateVehicle( arguments.tointeger(), 1, player.Pos, player.Angle, -1, -1 );
				}
				catch( err )
				{
					ClientMessage( "[#ed4242]Error: [#e9e9e9]Failed to create vehicle, " + err, player, 0, 0, 0 );
				}
			}
		}
		else if( command == "setcash" )
		{
			local plr;
			if( !arguments || NumTok( arguments, " " ) < 2 )
				ClientMessage( "[#ed4242]Usage: [#e9e9e9]/setcash [player] [cash]", player, 0, 0, 0 );
			else if( !( plr = GetPlayer( GetTok( arguments, " ", 1 ) ) ) )
				ClientMessage( "[#ed4242]Error: [#e9e9e9]That player is not online.")
			else
			{
				if( players.rawin( plr.ID ) )
				{
					local cash       = GetTok( arguments, " ", 2 );
					local pRemotePlr = players.rawget( plr.ID );
					pRemotePlr.SetCash( cash.tointeger() );

					ClientMessage( "[#80eb7a]Done.", player, 0, 0, 0 );
				}
			}
		}
	}
	else
		ClientMessage( "[#e80004]I don't know that command.", player, 0, 0, 0 );

	return 1;
}