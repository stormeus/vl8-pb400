function onPlayerSpawn( player )
{
	player.SpectateTarget = null;

	if( players.rawin( player.ID ) )
	{
		local pPlr = players.rawget( player.ID );
		if( pPlr )
			pPlr.GetSpawnWeapons();

		autoRespawn.rawset( player.ID, true );
	}

	Announce( player, "", 1 );
	if( !playerVehicles.rawin( player.ID ) )
		ClientMessage( "[#bae3ff]Remember that you can spawn your own personal vehicle with [#12d449]/veh", player, 0, 0, 0 );
	
	SPRITE_SPECHINT.HideFromPlayer(player);
	SPRITE_SPECTATEMENU.HideFromPlayer(player);
	player.SetWeapon(102,1);

	local angle = 0.0;
	switch( player.Skin )
	{
		/*case 73:  angle = -1.52440; break;
		case 51:  angle = 1.095380; break;
		case 53:  angle = -3.06520; break;
		case 57:  angle = -2.03807; break;
		case 1:   angle = -2.31957; break;
		case 74:  angle = -0.15000; break;
		case 81:  angle = 0.246000; break;
		case 112: angle = 216.3986; break;
		case 103: angle = 2.535940; break;
		case 93:  angle = 0.043010; break;
		case 61:  angle = -2.07135; break;
		case 156: angle = -1.89051; break;*/

		case 73:  angle = 272.682; break;
		case 51:  angle = 62.7388; break;
		case 53:  angle = 184.377; break;
		case 57:  angle = 243.232; break;
		case 1:   angle = 227.099; break;
		case 74:  angle = 351.741; break;
		case 81:  angle = 345.906; break;
		case 112: angle = 292.736; break;
		case 103: angle = 145.296; break;
		case 93:  angle = 2.46372; break;
		case 61:  angle = 241.318; break;
		case 156: angle = 251.682; break;
		default:  break;
	}

	player.Angle = angle;
}