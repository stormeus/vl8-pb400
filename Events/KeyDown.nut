function onKeyDown( player, bindID )
{
	local function IsCar( model )
	{
		switch( model )
		{
			// Bikes
			case 166:
			case 178:
			case 191:
			case 192:
			case 193:

			// Boats
			case 136:
			case 160:
			case 176:
			case 182:
			case 183:
			case 184:
			case 190:
			case 202:
			case 203:
			case 214:

			// Helicopters
			case 155:
			case 165:
			case 177:
			case 199:
			case 217:
			case 218:
			case 227:

			// RC vehicles
			case 171:
			case 194:
			case 195:
			case 231:
				return false;

			default:
				return true;
		}
	}
	switch( bindID )
	{
		case BIND_HYDRAULICS_LEFT:
		{
			if( player.Vehicle && IsCar( player.Vehicle.Model ) )
				player.Vehicle.AddRelTurnSpeed( Vector( 0.0, 0.035, 0.0 ) );

			return;
		}
		
		case BIND_HYDRAULICS_RIGHT:
		{
			if( player.Vehicle && IsCar( player.Vehicle.Model ) )
				player.Vehicle.AddRelTurnSpeed( Vector( 0.0, -0.035, 0.0 ) );

			return;
		}
		
		case BIND_HYDRAULICS_DOWN:
		{
			if( player.Vehicle && IsCar( player.Vehicle.Model ) )
				player.Vehicle.AddRelTurnSpeed( Vector( 0.06, 0.0, 0.0 ) );

			return;
		}
		
		case BIND_HYDRAULICS_UP:
		{
			if( player.Vehicle && IsCar( player.Vehicle.Model ) )
				player.Vehicle.AddRelTurnSpeed( Vector( -0.06, 0.0, 0.0 ) );

			return;
		}
		
		case BIND_CAR_BOOST:
		{
			if( player.Vehicle && IsCar( player.Vehicle.Model ) )
			{
				local vSpeed = player.Vehicle.Speed;
				vSpeed.x = vSpeed.x * 1.35;
				vSpeed.y = vSpeed.y * 1.35;
				vSpeed.z = vSpeed.z * 1.35;

				player.Vehicle.Speed = vSpeed;
			}

			return;
		}

		case BIND_SPAWN_SCREEN:
		{
			if( autoRespawn.rawin( player.ID ) )
			{
				autoRespawn.rawdelete( player.ID );
				ClientMessage( "[#d184d1]You will be sent to the spawn screen on next death.", player, 0, 0, 0 );
			}

			return;
		}

		case BIND_SPAWN_RAMP:
		{
			if( player.Vehicle && IsCar( player.Vehicle.Model ) )
			{
				local vSpeed = player.Vehicle.Speed;
				local vPos   = player.Vehicle.Pos;
				local objX   = vPos.x + (vSpeed.x * 20 );
				local objY   = vPos.y + (vSpeed.y * 20 );
				local objZ   = vPos.z - 0.1;
				local obj    = CreateObject( 562, player.World, Vector( objX, objY, objZ ), 255 );

				if( obj )
				{
					obj.RotateTo( player.Vehicle.Rotation, 0 );
					NewTimer( "WipeObject", 5000, 1, obj.ID );
				}
			}

			return;
		}

		case BIND_MENU_NEXT:
		{
			if( player.Vehicle && garageUsers.rawin( player.ID ) )
			{
				local pGarage = garageUsers.rawget( player.ID );
				pGarage.NextElem();
			}

			return;
		}

		case BIND_MENU_PREV:
		{
			if( player.Vehicle && garageUsers.rawin( player.ID ) )
			{
				local pGarage = garageUsers.rawget( player.ID );
				pGarage.PrevElem();
			}

			return;
		}

		case BIND_MENU_ACCEPT:
		{
			if( player.Vehicle && garageUsers.rawin( player.ID ) )
			{
				local pGarage = garageUsers.rawget( player.ID );
				pGarage.Select();
			}

			return;
		}

		case BIND_MENU_EXIT:
		{
			if( player.Vehicle && garageUsers.rawin( player.ID ) )
			{
				local pGarage = garageUsers.rawget( player.ID );
				pGarage.ExitPlayer();
			}

			return;
		}

		case BIND_VEH_COL2_NEXT:
		{
			if( player.Vehicle )
			{
				local col = player.Vehicle.Colour2;
				col++;

				if( col > 94 )
					col = 0;

				player.Vehicle.Colour2 = col;
			}

			return;
		}

		case BIND_VEH_COL2_PREV:
		{
			if( player.Vehicle )
			{
				local col = player.Vehicle.Colour2;
				col--;

				if( col < 0 )
					col = 94;

				player.Vehicle.Colour2 = col;
			}

			return;
		}

		case BIND_VEH_COL1_NEXT:
		case BIND_SPEC_NEXT:
		{
			if( player.Vehicle )
			{
				local col = player.Vehicle.Colour1;
				col++;

				if( col > 94 )
					col = 0;

				player.Vehicle.Colour1 = col;
			}
			else if( player.SpectateTarget != null && !player.Spawned )
			{
				local pTargetID = player.SpectateTarget.ID;
				local pTargetFind = pTargetID + 1;
				local pNewTarget = null;

				while(pNewTarget == null && pTargetFind != pTargetID)
				{
					if(pTargetFind > GetMaxPlayers() - 1)
						pTargetFind = 0;

					pNewTarget = FindPlayer(pTargetFind++);
					if(pNewTarget != null && !pNewTarget.Spawned)
						pNewTarget = null;
				}

				if(pNewTarget == null || pNewTarget.ID == pTargetID)
					ClientMessage( "[#ed4242]Error: [#e9e9e9]No one else can be spectated.", player, 0, 0, 0 );
				else
				{
					player.SpectateTarget = pNewTarget;
					local szMsg = "[#e9e9e9]You are now spectating [#80eb7a]" + pNewTarget.Name + " (" + pNewTarget.ID + ")";
					MessagePlayer(szMsg, player);
				}

				return;
			}

			return;
		}

		case BIND_VEH_COL1_PREV:
		case BIND_SPEC_PREV:
		{
			if( player.Vehicle )
			{
				local col = player.Vehicle.Colour1;
				col--;

				if( col < 0 )
					col = 94;

				player.Vehicle.Colour1 = col;
			}
			else if( player.SpectateTarget != null && !player.Spawned )
			{
				local pTargetID = player.SpectateTarget.ID;
				local pTargetFind = pTargetID - 1;
				local pNewTarget = null;

				while(pNewTarget == null && pTargetFind != pTargetID)
				{
					if(pTargetFind < 0)
						pTargetFind = GetMaxPlayers() - 1;

					pNewTarget = FindPlayer(pTargetFind--);
					if(pNewTarget != null && !pNewTarget.Spawned)
						pNewTarget = null;
				}

				if(pNewTarget == null || pNewTarget.ID == pTargetID)
					ClientMessage( "[#ed4242]Error: [#e9e9e9]No one else can be spectated.", player, 0, 0, 0 );
				else
				{
					player.SpectateTarget = pNewTarget;
					local szMsg = "[#e9e9e9]You are now spectating [#80eb7a]" + pNewTarget.Name + " (" + pNewTarget.ID + ")";
					MessagePlayer(szMsg, player);
				}
				
				return;
			}

			return;
		}

		case BIND_SPEC_TOGGLE:
		{
			if(!player.Spawned)
			{
				if(player.SpectateTarget != null)
				{
					SPRITE_SPECTATEMENU.HideFromPlayer(player);
					SPRITE_SPECHINT.ShowForPlayer(player);
					player.SpectateTarget = null;
				}
				else
				{
					local pTargetID = 0;
					local pNewTarget = null;
				
					while(pNewTarget == null && pTargetID < GetMaxPlayers())
					{
						pNewTarget = FindPlayer(pTargetID++);
						if(pNewTarget != null && !pNewTarget.Spawned)
							pNewTarget = null;
					}
				
					if(pNewTarget == null)
						ClientMessage( "[#ed4242]Error: [#e9e9e9]No one can be spectated at this time.", player, 0, 0, 0 );
					else
					{
						player.SpectateTarget = pNewTarget;
						local szMsg = "[#e9e9e9]You are now spectating [#80eb7a]" + pNewTarget.Name + " (" + pNewTarget.ID + ")";
						MessagePlayer(szMsg, player);

						SPRITE_SPECTATEMENU.ShowForPlayer(player);
						SPRITE_SPECHINT.HideFromPlayer(player);
					}
				}

				return;
			}

			return;
		}
	}
}

function ProcessSpecRemoval(player)
{
	if(player)
	{
		foreach(plr in players)
		{
			if(plr.pInstance.SpectateTarget && plr.pInstance.SpectateTarget.ID == player.ID)
			{
				local pTargetID = player.ID;
				local pTargetFind = pTargetID++;
				local pNewTarget = null;

				while(pNewTarget == null && pTargetFind != player.ID)
				{
					if(pTargetID >= GetMaxPlayers())
						pTargetID = 0;

					pNewTarget = FindPlayer(pTargetID++);
					if(pNewTarget != null && !pNewTarget.Spawned)
						pNewTarget = null;
				}

				if(pNewTarget == null)
				{
					plr.pInstance.SpectateTarget = null;

					ClientMessage( "[#ed4242]Error: [#e9e9e9]No one else can be spectated.", player.pInstance, 0, 0, 0 );
					SPRITE_SPECHINT.ShowToPlayer(plr.pInstance);
					SPRITE_SPECTATEMENU.HideFromPlayer(plr.pInstance);
				}
				else
					ClientMessage( "[#e9e9e9]You are now spectating [#80eb7a]" + pNewTarget.Name + " (" + pNewTarget.ID ")", plr.pInstance, 0, 0, 0 );
			}
		}
	}
}