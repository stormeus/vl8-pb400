// Vice Legends r8
// Written for 0.4 Public Beta #2 by Stormeus
// Edited further for 0.4 Public Beta #3 by Shadow
SCRIPT_LOC <- "Scripts/ViceLegends/";
DEBUGGING  <- true;

BIND_HYDRAULICS_LEFT  <- null;
BIND_HYDRAULICS_RIGHT <- null;
BIND_HYDRAULICS_UP    <- null;
BIND_HYDRAULICS_DOWN  <- null;
BIND_SPAWN_SCREEN     <- null;
BIND_CAR_BOOST        <- null;
BIND_SPAWN_RAMP       <- null;

BIND_MENU_NEXT        <- null;
BIND_MENU_PREV        <- null;
BIND_MENU_ACCEPT      <- null;
BIND_MENU_EXIT        <- null;

BIND_VEH_COL1_NEXT    <- null;
BIND_VEH_COL1_PREV    <- null;
BIND_VEH_COL2_NEXT    <- null;
BIND_VEH_COL2_PREV    <- null;

BIND_SPEC_TOGGLE      <- null;
BIND_SPEC_NEXT        <- null;
BIND_SPEC_PREV        <- null;

SPRITE_SPECHINT       <- null;
SPRITE_SPECTATEMENU   <- null;
SPRITE_LOGO           <- null;

function onScriptLoad()
{
	print( "Loading VL8..." );
	dofile( SCRIPT_LOC + "LibMisc.nut" );
//	dofile( SCRIPT_LOC + "Ramps.nut" );
	dofile( SCRIPT_LOC + "Vehicles.nut" );
	
		LoadVehicles();
//		CreateStuntEnvironment();
	dofile( SCRIPT_LOC + "CGarage.nut" );
	{
		CGarage( Vector( -1007.54, -835.820, 7.36511 ), Vector( -1007.85, -850.627, 7.36500 ), Vector( -1013.21, -856.665, 9.36500 ) );
		CGarage( Vector( -912.511, -1270.39, 11.7915 ), Vector( -906.155, -1255.84, 11.7161 ), Vector( -900.697, -1250.51, 13.7179 ) );
		CGarage( Vector( -869.397, -119.314, 11.0924 ), Vector( -880.174, -114.036, 11.1034 ), Vector( -882.313, -109.393, 13.1034 ) );
		CGarage( Vector( 329.7850, 429.7420, 11.3942 ), Vector( 319.5110, 432.6150, 11.2364 ), Vector( 316.6340, 436.3600, 13.3007 ) );
		CGarage( Vector( -12.0577, -1257.96, 10.4633 ), Vector( -4.02264, -1257.39, 10.4633 ), Vector( -3.86713, -1260.03, 12.4633 ) );
	}

	dofile( SCRIPT_LOC + "CPlayer.nut" );
	dofile( SCRIPT_LOC + "CTeleport.nut" );
	{
//		CTeleport( Vector( -811.567, 1354.06, 66.4675 ), Vector( -830.662, 1312.54, 11.5492 ) );
//		CTeleport( Vector( -448.277, 1250.78, 77.4093 ), Vector( -448, 1251.53, 11.767 ) );
//		CTeleport( Vector( -451.262, 1128.05, 56.6909 ), Vector( -410.124, 1120.78, 11.1456 ) );
	}

	dofile( SCRIPT_LOC + "LMS.nut" );
	dofile( SCRIPT_LOC + "RandomMessages.nut" );
	dofile( SCRIPT_LOC + "Events/KeyDown.nut" );
	{
		BIND_HYDRAULICS_LEFT  = BindKey( true, 0x64, 0, 0 );
		BIND_HYDRAULICS_RIGHT = BindKey( true, 0x66, 0, 0 );
		BIND_HYDRAULICS_UP    = BindKey( true, 0x68, 0, 0 );
		BIND_HYDRAULICS_DOWN  = BindKey( true, 0x62, 0, 0 );
		BIND_SPAWN_SCREEN     = BindKey( true, 0x78, 0, 0 );
		BIND_CAR_BOOST        = BindKey( true, 0x77, 0, 0 );
		BIND_SPAWN_RAMP       = BindKey( true, 0x76, 0, 0 );

		BIND_MENU_NEXT        = BindKey( true, 0x28, 0, 0 );
		BIND_MENU_PREV        = BindKey( true, 0x26, 0, 0 );
		BIND_MENU_ACCEPT      = BindKey( true, 0x20, 0, 0 );
		BIND_MENU_EXIT        = BindKey( true, 0x11, 0, 0 );

		BIND_VEH_COL2_NEXT    = BindKey( true, 0x27, 0x11, 0 );
		BIND_VEH_COL2_PREV    = BindKey( true, 0x25, 0x11, 0 );
		BIND_VEH_COL1_NEXT    = BindKey( true, 0x27, 0, 0 );
		BIND_VEH_COL1_PREV    = BindKey( true, 0x25, 0, 0 );

		BIND_SPEC_TOGGLE      = BindKey( true, 0x53, 0, 0 );
		BIND_SPEC_NEXT        = BIND_VEH_COL1_NEXT; // Shared key: right arrow
		BIND_SPEC_PREV        = BIND_VEH_COL1_PREV; // Shared key: left arrow
	}

	dofile( SCRIPT_LOC + "Events/PickupPickedUp.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerChat.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerCommand.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerDeath.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerJoin.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerPart.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerRequestClass.nut" );
	dofile( SCRIPT_LOC + "Events/PlayerSpawn.nut" );
	dofile( SCRIPT_LOC + "Events/Crashes.nut" );

	SetTaxiBoostJump    ( true );
	SetDrivebyEnabled   ( false );
	SetFrameLimiter     ( true );
	SetSyncFrameLimiter ( true );
	//SetFriendlyFire     ( false );
	SetWastedSettings( 650, 250, 0.35, 0.35, RGB( 0, 0, 0 ), 5000, 3000 );

	//ReplaceMapObject( 3013, Vector( -7.80468, -1257.64, 10.8187 ), Vector( 0.0, 0.0, 0.000 ) );
	//ReplaceMapObject( 1574, Vector( -874.696, -116.696, 11.9875 ), Vector( 0.0, 0.0, 0.000 ) );
	//ReplaceMapObject( 1393, Vector( -1007.95, -841.779, 8.59442 ), Vector( 0.0, 0.0, 0.000 ) );
	//ReplaceMapObject( 4145, Vector( 325.0830, 431.1370, 11.5872 ), Vector( 180, 180, 342.5 ) );
	//ReplaceMapObject( 4142, Vector( 326.0970, 438.7180, 14.6737 ), Vector( 180, 180, 342.5 ) );
	

	//RawHideMapObject( 4142, 3260, 4387, 146 );
	//RawHideMapObject( 4145, 3250, 4311, 115 );
	//ReplaceMapObject( 0828, Vector( -910.002, -1264.71, 12.4923 ), Vector( 0.0, 0.0, 0.000 ) );
	
	RawHideMapObject( 1867, -4604, 10569, 221 ); // doontoon21
	RawHideMapObject( 1474, -8568, -797, 127 ); // drugstoreint
	RawHideMapObject( 1475, -8563, -800, 126 ); // drugstoreext
 
    // Radio streams
	CreateRadioStream(11,"KICKRADIO", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=164745", true);
	CreateRadioStream(12,"80s COUNTRY", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=9494745", true);
	CreateRadioStream(13,"HARD ROCK HEAVEN", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=106750", true);
	CreateRadioStream(14,"BIG R POP MIX 80 & 90s", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=66192", true);
	CreateRadioStream(15,"Sky City Radio", "http://radio.sc-mp.net:8000/stream.m3u", true);
	// -------------
	
	CreateMarker(0, Vector(100,100,100), 0, RGBA(255,255,0,255) , 100);
	CreateMarker(0, Vector(100,100,100), 0, RGBA(255,0,255,255) , 101);
	CreateObject( 6000, 1, Vector( 35.41, 991.72, 9.78 ), 255 ).RotateToEuler( Vector( 0.0, 0.0, 3.5 ), 0 ); // cannon
	
	//SetWeaponDataValue(33, 2, 1.0); // FieldRange
	//SetWeaponDataValue(33, 3, 2500); // FieldFiringRate
	//SetWeaponDataValue(33, 4, 450); // FieldReload
	//SetWeaponDataValue(33, 5, 1); // FieldClipSize
	SetWeaponDataValue(33, 6, 0.001); // FieldDamage
	/*SetWeaponDataValue(33, 15, 15); // FieldAnimGroup
	SetWeaponDataValue(33, 16, 12.0); // FieldAnimLoopStart
	SetWeaponDataValue(33, 17, 34.0); // FieldAnimLoopEnd
	SetWeaponDataValue(33, 18, 15.0); // FieldAnimFirePos
	SetWeaponDataValue(33, 19, 9.0); // FieldAnimTwoLoopStart
	SetWeaponDataValue(33, 20, 26.0); // FieldAnimTwoLoopEnd
	SetWeaponDataValue(33, 21, 13.0); // FieldAnimTwoFirePos
	SetWeaponDataValue(33, 22, 30.0); // FieldAnimBreakoutPos
	SetWeaponDataValue(33, 23, 277); // FieldModelId
	SetWeaponDataValue(33, 24, -1); // FieldModelTwoId*/
	//SetWeaponDataValue(33, 25, 0x020040); // FieldFlags
	//SetWeaponDataValue(33, 25, 0x000200); // FieldFlags
	//SetWeaponDataValue(33, 26, 7); // FieldWeaponSlot

	SPRITE_SPECHINT = CreateSprite("hint_spectate.png", -150, -384, 0, 0, 0, 255);
	SPRITE_SPECTATEMENU = CreateSprite("hint_specmenu.png", -220, -384, 0, 0, 0, 255);
	SPRITE_LOGO = CreateSprite("logo.png", -123, -69, 0, 0, 0, 255);
	SetUseClasses(true);

	/*local string = "";
	local length = 2048;
	local i = 0;
	while(i < length)
	{
		string = string + i.tostring();
		i += i.tostring().len();
	}

	print(string);*/

	if(time() < 1390737600)
	{
		SetPassword("beta");
		NewTimer("Unlock", 1000, 0);
	}
}

function ReplaceMapObject( model, vec, rot )
{
	HideMapObject( model, vec.x, vec.y, vec.z );

	local o = CreateObject( model, 1, vec, 255 );
	o.RotateToEuler( rot, 0 );

	return o;
}

function Unlock()
{
	if(time() >= 1390737600)
		SetPassword("");
}
