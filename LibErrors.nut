// Handles errors. Pretty self-explanatory
function errorHandling( obj )
{
	// Get the stack info we need to log
	local callingStack = getstackinfos( 2 );
	
	// Get the date to seed rand()
	local cDate = date();
	
	// Generate a seed
	local seed = ( ( time() + rand() ) / 17 ).tointeger();

	// And then seed our seed
	seed += ( cDate.sec * cDate.hour );
	
	// Seed rand()
	srand( seed );
	
	// Generate a random ID for this error
	local ID = cDate.sec + "-" + rand();
	
	// And record them to their own variables
	local funcName = callingStack.func;
	local origin   = callingStack.src;
	local lineNo   = callingStack.line;
	local locals   = "";
	
	// Get all the locals we can
	foreach( index, value in callingStack.locals )
	{
		if( index != "this" )
			locals = locals + "\t[" + index + "]: " + value + "\n";
	}
	
	// Generate an error log filename
	local filePath = DATA_LOC + "Errors/" + ID + ".log";
	
	// Write to our error log
	TXTAddLine( filePath, "Error occurring in " + origin + ":" + lineNo + ", function " + funcName + "." );
	TXTAddLine( filePath, ">> " + obj + "\n" );
	TXTAddLine( filePath, locals );
	
	// If we're in debugging mode (a la beta),
	// trigger a global message.
	if( DEBUGGING == true && Message )
	{
		Message( "An error occurred: contact a developer. (" + ID + ")" );
		print( "An error has been logged to ID: " + ID );
	}
}

seterrorhandler( errorHandling );