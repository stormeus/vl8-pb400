garages       <- {};
garageUsers   <- {};
garageEntries <- {};

enum PNSACTIONS
{
	ID_FIX,
	ID_MORETRACTION,
	ID_MOREACCEL,
	ID_MORESPEED,
	ID_BETTERBRAKE,
	ID_LESSDAMAGE,
	ID_AQUATIC
};

class CGarage
{
	menuElems = [
		[ PNSACTIONS.ID_FIX,          250,  "Basic Repair"            ],
		[ PNSACTIONS.ID_MORETRACTION, 500,  "Increase Traction"       ],
		[ PNSACTIONS.ID_MOREACCEL,    750,  "Increase Acceleration"   ],
		[ PNSACTIONS.ID_MORESPEED,    750,  "Increase Top Speed"      ],
		[ PNSACTIONS.ID_BETTERBRAKE,  600,  "Better Brakes"           ],
		[ PNSACTIONS.ID_LESSDAMAGE,   1250, "Less Collision Damage"   ],
		[ PNSACTIONS.ID_AQUATIC,      2500, "Drive on Water"          ]
	];

	user        = null;
	outerCP     = null;
	innerCP     = null;
	camPos      = null;
	currentElem = 0;

	constructor( outPos, inPos, cameraPos )
	{
		this.outerCP   = ::CreatePickup( 382, 1, 1, outPos, 255, false );
		this.innerCP   = inPos;
		this.camPos    = cameraPos;

		garages.rawset( this.outerCP.ID, this );
	}

	function EnterPlayer( player )
	{
		if( this.user != null )
			return false;
		else if( !player.Vehicle )
			return false;
		else if( player.Vehicle.Driver.ID != player.ID )
			return false;
		else
		{
			::garageUsers.rawset( player.ID, this );
			player.SetCameraPos( this.camPos, this.innerCP );

			player.Vehicle.Pos = this.innerCP;
			player.IsFrozen    = true;
			this.user          = player;
			this.currentElem   = 0;

			::ClientMessage( "[#70d478]Use the UP and DOWN arrow keys to navigate the menu.", player, 0, 0, 0 );
			::ClientMessage( "[#70d478]Press CTRL to exit. Press SPACE to select.", player, 0, 0, 0 );
			this.RenderMenu();

			return true;
		}
	}

	function DepartPlayer()
	{
		if( this.user == null )
			return false;
		else if( !this.user.Vehicle )
			return false;
		else
		{
			if( ::garageUsers.rawin( this.user.ID ) )
				::garageUsers.rawdelete( this.user.ID );

			this.user.Vehicle.Respawn();
			return true;
		}
	}

	function ExitPlayer()
	{
		if( this.user == null )
			return false;
		else if( !this.user.Vehicle )
			return false;
		else
		{
			if( ::garageUsers.rawin( this.user.ID ) )
				::garageUsers.rawdelete( this.user.ID );
			
			::garageEntries.rawset( this.user.ID, ::time() );
			::Announce( this.user, "\x10", 1 );

			this.user.RestoreCamera();
			this.user.Vehicle.Pos = this.outerCP.Pos;
			this.user.IsFrozen    = false;
			this.user             = null;

			return true;
		}
	}

	function RenderMenu()
	{
		if( this.user )
		{
			local prevElem = this.currentElem - 1;
			local nextElem = this.currentElem + 1;

			if( prevElem < 0 )
				prevElem = this.menuElems.len() - 1;

			if( nextElem > this.menuElems.len() - 1 )
				nextElem = 0;

			local msg = format(
				"~w~%s ($%d) \x10 ~y~%s ($%d) \x10 ~w~%s ($%d)",
				this.menuElems[prevElem][2],
				this.menuElems[prevElem][1],
				this.menuElems[this.currentElem][2],
				this.menuElems[this.currentElem][1],
				this.menuElems[nextElem][2],
				this.menuElems[nextElem][1]
			);

			::Announce( this.user, msg, 1 )
			//::Announce( this.user, msg, 8 )
		}
	}

	function PrevElem()
	{
		local prevElem = this.currentElem - 1;
		if( prevElem < 0 )
			prevElem = this.menuElems.len() - 1;

		this.currentElem = prevElem;
		this.RenderMenu();
	}

	function NextElem()
	{
		local nextElem = this.currentElem + 1;
		if( nextElem > this.menuElems.len() - 1 )
			nextElem = 0;

		this.currentElem = nextElem;
		this.RenderMenu();
	}

	function Select()
	{
		if( this.user && players.rawin( this.user.ID ) )
		{
			local pPlr = players.rawget( this.user.ID );
			if( pPlr.cash < this.menuElems[this.currentElem][1] )
			{
				ClientMessage( "[#ed4242]You cannot afford that.", this.user, 0, 0, 0 );
				return false;
			}
			else
			{
				pPlr.AddCash( -this.menuElems[this.currentElem][1] );
				switch( this.menuElems[this.currentElem][0] )
				{
					case PNSACTIONS.ID_FIX:
						this.user.Vehicle.Health = 1000;
						this.user.Vehicle.Damage = 0;

						::ClientMessage( "[#70d478]You have fixed your car.", this.user, 0, 0, 0 );
						break;

					case PNSACTIONS.ID_MORETRACTION:
						local traction = this.user.Vehicle.GetHandlingData( 9 );
						      traction = traction * 1.4;

						this.user.Vehicle.SetHandlingData( 9, traction );
						::ClientMessage( "[#70d478]Traction increased.", this.user, 0, 0, 0 );
						break;

					case PNSACTIONS.ID_MOREACCEL:
						local accel = this.user.Vehicle.GetHandlingData( 14 );
						      accel = accel * 1.4;

						this.user.Vehicle.SetHandlingData( 14, accel );
						::ClientMessage( "[#70d478]Acceleration increased.", this.user, 0, 0, 0 );
						break;

					case PNSACTIONS.ID_MORESPEED:
						local speed = this.user.Vehicle.GetHandlingData( 13 );
						      speed = speed + 25.0;

						this.user.Vehicle.SetHandlingData( 13, speed );
						::ClientMessage( "[#70d478]Top speed increased by 25km/h.", this.user, 0, 0, 0 );
						break;
					
					case PNSACTIONS.ID_BETTERBRAKE:
						local brake = this.user.Vehicle.GetHandlingData( 17 );
						      brake = brake * 1.42;

						this.user.Vehicle.SetHandlingData( 17, brake );
						::ClientMessage( "[#70d478]Brakes upgraded.", this.user, 0, 0, 0 );
						break;
					
					case PNSACTIONS.ID_LESSDAMAGE:
						local mult = this.user.Vehicle.GetHandlingData( 23 );
						      mult = mult * 0.65;

						this.user.Vehicle.SetHandlingData( 23, mult );
						::ClientMessage( "[#70d478]Collision damage protection upgraded.", this.user, 0, 0, 0 );
						break;
					
					case PNSACTIONS.ID_AQUATIC:
						this.user.Vehicle.SetHandlingData( 33, 1.0 );
						::ClientMessage( "[#70d478]Your car can now drive on water.", this.user, 0, 0, 0 );

						break;
					
					default:
						break;
				}

				return true;
			}
		}
		else
			return false;
	}
};