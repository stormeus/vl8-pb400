// Tokenization functions not provided by the server itself
function GetTok(string, separator, n, ...)
{
	local m = vargv.len() > 0 ? vargv[0] : n,
		  tokenized = split(string, separator),
		  text = "";
	
	if (n > tokenized.len() || n < 1) return null;
	for (; n <= m; n++)
	{
		text += text == "" ? tokenized[n-1] : separator + tokenized[n-1];
	}
	return text;
}

function NumTok(string, separator)
{
	local tokenized = split(string, separator);
	return tokenized.len();
}

// http://forum.iv-multiplayer.com/index.php/topic,2643.msg26175.html#msg26175
function random( min = 0, max = RAND_MAX )
{
	local r = rand();
	srand(GetTickCount() * r);
	return (r % ((max + 1) - min)) + min;
}

// Provides logging functionality
function TXTAddLine(filename, text)
{
	local fhnd = file(filename, "a+");
	foreach(char in text)
		fhnd.writen(char, 'c');	
	
	fhnd.writen('\n', 'c');
	fhnd=null;
}

// Alternative to FindPlayer
function GetPlayer( player )
{
	local plr;
	if( ( plr = FindPlayer( player ) ) )
		return plr;
	else
	{
		try
		{
			plr = FindPlayer( player.tointeger() );
			if( plr )
				return plr;
			else
				return null;
		}
		catch( error )
			return null;
	}
}

function WipeObject( objID )
{
	local obj = FindObject( objID );
	if( obj )
		obj.Delete();
}

function Heal( playerID )
{
	local plr = FindPlayer( playerID );
	if( plr && plr.IsSpawned )
	{
		plr.Health = 100;
		ClientMessage( "[#80eb7a]You have been healed successfully.", plr, 0, 0, 0 );
	}
}

function Teleport( playerID, x, y, z )
{
	local plr = FindPlayer( playerID );
	if( plr && plr.IsSpawned )
		plr.Pos = Vector( x, y, z );
}

// By Juppi
function GetDistance( v1, v2 )
{
	return sqrt( (v2.x-v1.x)*(v2.x-v1.x) + (v2.y-v1.y)*(v2.y-v1.y) + (v2.z-v1.z)*(v2.z-v1.z) );
}